import React, { Component } from "react";

const Context = React.createContext();

const reducer = (state, action) => {
  switch (action.type) {
    case "DELETE_CONTACT":
      return {
        ...state,
        contacts: state.contacts.filter(
          contact => contact.id !== action.payload
        )
      };
    case "ADD_CONTACT":
      return {
        ...state,
        contacts:[action.payload, ...state.contacts]
    };
    default:
      return state;
  }
};

export class Provider extends Component {
  state = {
    contacts: [
      {
        id: 1,
        name: "Jhonny Josmel Ortiz",
        email: "Jhonny@gmail.com",
        phone: "809-219-4549"
      },
      {
        id: 2,
        name: "Yefry Bautista",
        email: "Yefry@gmail.com",
        phone: "849-341-8863"
      },
      {
        id: 3,
        name: "Ralph Severino",
        email: "Ralph@gmail.com",
        phone: "829-618-9271"
      }
    ],
    dispatch: action => this.setState(state => reducer(state, action))
  };
  render() {
    return (
      <Context.Provider value={this.state}>
        {this.props.children}
      </Context.Provider>
    );
  }
}
export const Consumer = Context.Consumer;
