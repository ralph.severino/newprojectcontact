import React from "react";
import Header from "./components/Layout/Header";
import "bootstrap/dist/css/bootstrap.min.css";
import Contacts from "./components/Contacts/Contacts";
import AddContact from "./components/Contacts/AddContact";
import About from './components/pages/About'
import { Provider } from "./context";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import NotFound from "./components/pages/NotFound";
function App() {
  return (
    <Provider>
      <Router>
        <div className="App">
          <Header branding="Contact Manager" />
          <div className="container">
            <Switch>
              <Route exact path="/" component={Contacts} />
              <Route exact path="/about" component={About} />
              <Route exact path="/contact/add" component={AddContact} />
              <Route component={NotFound}/>
            </Switch>
          </div>
        </div>
      </Router>
    </Provider>
  );
}

export default App;
